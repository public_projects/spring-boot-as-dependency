package com.example;

import com.example.b.NewClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AApplication.class, args);
	}

	@Autowired
	private NewClass newClass;

	@Override
	public void run(String... args) throws Exception {
		System.err.println(newClass.getValue());
	}
}
